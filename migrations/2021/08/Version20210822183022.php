<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210822183022 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'shorted-link.overview', 'hash' => 'f8582efceeb456804408dfd25fa73308', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkrácené odkazy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'shorted-link.overview.title', 'hash' => '3052edc9374c6c734c2e2e819ce9d24f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkrácené odkazy|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.action.inline-add', 'hash' => '7b112ead5c2208ad69757ee577751c7c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit zkrácený odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.form.name.req', 'hash' => '71a44613590f0ca206df39ede146d9e3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.form.link.req', 'hash' => '0d9a2091469185ca4bdabbb55cd818d1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.form.shorted-link.req', 'hash' => 'b92665bc16867d671890810a85a786e1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím zkratku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.overview.is-active', 'hash' => '91eef992521f363ddc7fe7cbd655803e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.overview.name', 'hash' => '4e6d41e075498d8a55f01651b3edd8d6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.overview.link', 'hash' => '7c2ba89f55f4d09b126ffc92e21a591a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.overview.shorted-link', 'hash' => '32046825d40cf79c565806d2e9b3fcef', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkrácený odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.action.flash.inline-add.success', 'hash' => '0582e71482fea41294415236637da609', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkrácený odkaz byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.action.flash.inline-edit.success', 'hash' => 'e66e5b21c1b5c5896a8a7a0bc467a093', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkrácený odkaz byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.shorted-link.overview.counter.title', 'hash' => '933109b83742ed6c077883e8cb8a39db', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet použití zkráceného odkazu', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
