<?php

declare(strict_types=1);

namespace Skadmin\ShortedLink\Doctrine\ShortedLink;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;
use function count;
use function sprintf;

final class ShortedLinkFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ShortedLink::class;
    }

    public function create(string $name, string $link, string $shortedLink, bool $isActive): ShortedLink
    {
        return $this->update(null, $name, $link, $shortedLink, $isActive);
    }

    public function update(?int $id, string $name, string $link, string $shortedLink, bool $isActive): ShortedLink
    {
        $sh = $this->get($id);

        $excludes = [];
        if ($sh->isLoaded()) {
            $excludes[] = $sh;
        }

        $sh->update($name, $link, $this->getValidShortedLink($shortedLink, $excludes), $isActive);

        $this->em->persist($sh);
        $this->em->flush();

        return $sh;
    }

    public function incrementCounter(?int $id): ShortedLink
    {
        $shortedLink = $this->get($id);
        $shortedLink->incrementCounter();

        $this->em->flush();

        return $shortedLink;
    }

    public function get(?int $id = null): ShortedLink
    {
        if ($id === null) {
            return new ShortedLink();
        }

        $shortedLink = parent::get($id);

        if ($shortedLink === null) {
            return new ShortedLink();
        }

        return $shortedLink;
    }

    public function findByShortedLink(string $shortedLink, array $excludes = []): ?ShortedLink
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('shortedLink', $shortedLink));

        if (count($excludes) > 0) {
            $criteria->andWhere(Criteria::expr()->notIn('id', $excludes));
        }

        $results = $repository->createQueryBuilder('a')
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();

        if (count($results) > 0) {
            return $results[0];
        }

        return null;
    }

    public function getValidShortedLink(string $shortedLink, array $excludes = []): string
    {
        $cnt               = 1;
        $modifyShortedLink = $shortedLink;

        while ($this->findByShortedLink($modifyShortedLink, $excludes) instanceof ShortedLink) {
            $modifyShortedLink = sprintf('%s-%d', $shortedLink, $cnt++);
        }

        return $modifyShortedLink;
    }
}
