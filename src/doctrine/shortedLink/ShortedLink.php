<?php

declare(strict_types=1);

namespace Skadmin\ShortedLink\Doctrine\ShortedLink;

use Doctrine\ORM\Mapping as ORM;
use Nette\Http\Url;
use SkadminUtils\DoctrineTraits\Entity;

use function sprintf;
use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ShortedLink
{
    use Entity\Id;
    use Entity\Link;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\Counter;

    #[ORM\Column]
    private string $shortedLink;

    public function __construct()
    {
        $this->counter = 0;
    }

    public function update(string $name, string $link, string $shortedLink, bool $isActive): void
    {
        $this->name        = $name;
        $this->shortedLink = $shortedLink;
        $this->isActive    = $isActive;

        $link = new Url($link);

        if (trim($link->getScheme()) === '') {
            $link->setScheme('http');
            $link->setPath(sprintf('//%s', $link->getPath()));
        }

        $this->link = $link->getAbsoluteUrl();
    }

    public function getShortedLink(): string
    {
        return $this->shortedLink;
    }

    public function incrementCounter(): void
    {
        $this->counter++;
    }
}
