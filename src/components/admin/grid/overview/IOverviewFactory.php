<?php

declare(strict_types=1);

namespace Skadmin\ShortedLink\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
