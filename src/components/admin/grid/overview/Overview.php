<?php

declare(strict_types=1);

namespace Skadmin\ShortedLink\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\ShortedLink\BaseControl;
use Skadmin\ShortedLink\Doctrine\ShortedLink\ShortedLink;
use Skadmin\ShortedLink\Doctrine\ShortedLink\ShortedLinkFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use SkadminUtils\Utils\Utils\Strings;
use function boolval;
use function intval;
use function sprintf;
use function str_replace;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private ShortedLinkFacade $facade;

    public function __construct(ShortedLinkFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'shorted-link.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.id', 'DESC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.shorted-link.overview.name')
            ->setRenderer(function (ShortedLink $shortedLink): Html {
                $href = $this->getPresenter()->link('//:ShortedLink:Redirect:', ['shortedLink' => $shortedLink->getShortedLink()]);

                $link = Html::el('code', ['class' => 'text-muted d-block small'])
                    ->setText(str_replace(['https://', 'http://', 'www.'], '', $href));

                $counter = Html::el('small', [
                    'class'       => 'text-muted',
                    'title'       => $this->translator->translate('grid.shorted-link.overview.counter.title'),
                    'data-toggle' => 'tooltip',
                ])->setText(sprintf('[%s]', $shortedLink->getCounter()));

                $render = new Html();
                $render->addHtml(sprintf('%s %s', $shortedLink->getName(), $counter))
                    ->addHtml($link);

                return $render;
            });
        $grid->addColumnText('link', 'grid.shorted-link.overview.link')
            ->setRenderer(static function (ShortedLink $shortedLink): Html {
                $render = Html::el();

                $link = Strings::truncate($shortedLink->getLink(), 50);
                $render->addHtml(
                    Html::el('a', [
                        'href'   => $shortedLink->getLink(),
                        'target' => '_blank',
                    ])->addText($link)
                );

                if ($link !== $shortedLink->getLink()) {
                    $render->addHtml(
                        Html::el('i', [
                            'class'          => 'ml-2 fas fa-fw fa-info-circle',
                            'title'          => $shortedLink->getLink(),
                            'data-placement' => 'left',
                            'data-toggle'    => 'tooltip',
                        ])
                    );
                }

                return $render;
            });
        $grid->addColumnText('shortedLink', 'grid.shorted-link.overview.shorted-link');
        $this->addColumnIsActive($grid, 'shorted-link.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.shorted-link.overview.name');
        $grid->addFilterText('link', 'grid.shorted-link.overview.link');
        $grid->addFilterText('shortedLink', 'grid.shorted-link.overview.shorted-link');
        $this->addFilterIsActive($grid, 'shorted-link.overview');

        // OTHER
        $grid->setDefaultFilter(['isActive' => true]);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.shorted-link.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.shorted-link.form.name')
            ->setRequired('grid.shorted-link.form.name.req');
        $container->addText('link', 'grid.shorted-link.form.link')
            ->setRequired('grid.shorted-link.form.link.req');
        $container->addText('shortedLink', 'grid.shorted-link.form.shorted-link')
            ->setDefaultValue(Random::generate())
            ->setRequired('grid.shorted-link.form.shorted-link.req');
        $container->addSelect('isActive', 'grid.shorted-link.form.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $this->facade->create($values->name, $values->link, $values->shortedLink, boolval($values->isActive));
        $this->getPresenter()->flashMessage('grid.shorted-link.action.flash.inline-add.success', Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $container->addText('name', 'grid.shorted-link.form.name')
            ->setRequired('grid.shorted-link.form.name.req');
        $container->addText('link', 'grid.shorted-link.form.link')
            ->setRequired('grid.shorted-link.form.link.req');
        $container->addText('shortedLink', 'grid.shorted-link.form.shorted-link')
            ->setRequired('grid.shorted-link.form.shorted-link.req');
        $container->addSelect('isActive', 'grid.shorted-link.form.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    public function onInlineEditDefaults(Container $container, ShortedLink $shortedLink): void
    {
        $container->setDefaults([
            'name'        => $shortedLink->getName(),
            'link'        => $shortedLink->getLink(),
            'shortedLink' => $shortedLink->getShortedLink(),
            'isActive'    => intval($shortedLink->isActive()),
        ]);
    }

    /**
     * @param int|string $id
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit($id, ArrayHash $values): void
    {
        $this->facade->update(intval($id), $values->name, $values->link, $values->shortedLink, boolval($values->isActive));
        $this->getPresenter()->flashMessage('grid.shorted-link.action.flash.inline-edit.success', Flash::SUCCESS);
    }
}
